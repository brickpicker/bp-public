<cfcomponent name="geoPlugin" output="false">

<!--- Pseudo-constructor --->
<cfset variables.datasource 	= application.datasource />
<cfset variables.targetKey 	= "<enter key here>"/>
<cfset variables.prodstatus 	= "api.target.com"/>
<cfset this.secureJSON = false />


    <cffunction name="ipLocation" access="remote" returntype="any" displayname="ipLocation" output="no">


        <cfargument name="ip" type="string" required="no" default="">
        
        <cfset var local = StructNew()>     
        
        <cftry>
        
            <cfset local.ip = Trim(arguments.ip)>

            <cfset local.url = "https://geoip.maxmind.com/geoip/v2.1/city/#local.ip#">
            <cfhttp result="local.ipRequest" url="#local.url#" method="get" timeout="5" throwOnError="yes" username="" password="" />
            
            
            <cfif isJSON(local.ipRequest.filecontent) EQ "No">
                <cfthrow message="geoPlugin response failed">
            </cfif>
            
            <cfset var results = queryNew("city,longitude,latitude,maxmind,postcode,country")>
            
            <cfset result = deserializeJSON(local.ipRequest.filecontent)>
            
            <cfset maxmindCity = result.city.names.en>
            <cfset maxmindLat = result.location.latitude>
            <cfset maxmindLong = result.location.longitude>
            <cfset maxmindCount = result.maxmind.queries_remaining>
            <cfset maxmindZip = result.postal.code>
            <cfset maxmindCountry = result.country.names.en>
            
            <cfset queryAddRow(results)>
			<cfset querySetCell(results, "city", maxmindCity)>
			<cfset querySetCell(results, "longitude", maxmindLong)>
			<cfset querySetCell(results, "latitude", maxmindLat)>
			<cfset querySetCell(results, "maxmind", maxmindCount)>
			<cfset querySetCell(results, "postcode", maxmindZip)>
			<cfset querySetCell(results, "country", maxmindCountry)>
			
			
            <cfreturn results>

                    
            <cfcatch>        
                <cfset local.return = StructNew()>
                <cfset local.return["status"] = "BAD">
                <cfset local.return["cfcatch"] = cfcatch>
                <cfreturn local.return>
            </cfcatch>
                    
        </cftry>

	</cffunction>
	
	
	<cffunction name="getLatestSearches" access="remote" output="false" returns="query">

	    <cfquery name="getData" datasource="#application.datasource#">
	        SELECT distinct DCPI, product_name
	        FROM target_dcpi
	        WHERE product_name != 'N/A'
	        ORDER BY create_dt DESC
	        LIMIT 10
	    </cfquery>
	
	
	    <cfreturn getData />
	    
	</cffunction>
	
	
	<cffunction name="getTopSearches" access="remote" output="false" returns="query">

	    <cfquery name="getData" datasource="#application.datasource#">
	        SELECT count(dcpi) as totalDCPI, DCPI, product_name
	        FROM target_dcpi
	        WHERE create_dt >= now() - INTERVAL 1 DAY
	        GROUP by dcpi, product_name
	        ORDER by totalDCPI DESC
	        LIMIT 10
	    </cfquery>
	
	
	    <cfreturn getData />
	    
	</cffunction>
	
	
	<cffunction name="getGeocode" access="remote" output="false" returnType="query">
	<cfargument name="postcode" type="string" required="true">
		
		
	
	    <cfhttp url="http://#variables.prodstatus#/v2/location/geocode?place=#trim(arguments.postcode)#&key=#variables.targetKey#&limit=1" result="searchResults" method="GET" charset="utf-8">
			<cfhttpparam type="header" name="Accept" value="application/json">
		</cfhttp>
		
		<cfset var results = queryNew("longitude,latitude")>

		<cfset result = deserializeJSON(searchResults.filecontent) >
		
		<cfset totalRecords = val(result.Locations['@count'])>
		
		<cfif totalRecords gt 0>
		<cfset entry = result.Locations.Location>
		<cfset queryAddRow(results)>
		<cfset querySetCell(results, "latitude", entry.Address.Latitude)>
		<cfset querySetCell(results, "longitude", entry.Address.Longitude)>
	    <cfreturn results />
	    
	    <cfelse>
	    
	    <cfset results = results>
		<cfreturn results />
	    
		</cfif>
	    
	</cffunction>
	
	
	<cffunction name="getAllLocations" access="remote" output="false" returnType="any">

	    <cfhttp url="http://api.target.com/v2/location?key=#variables.targetKey#" result="searchResults" method="GET" timeout="20000">
			<cfhttpparam type="header" name="Accept" value="application/xml">
		</cfhttp>
	
		<cfset result = xmlParse(searchResults.filecontent) >

		<cfreturn result />

	    
	</cffunction>
	
	<cffunction name="getTargetGeo" access="remote" output="false" returnType="any">
		<cfargument name="store_id" type="numeric" required="true">
		
	    <cfquery name="qResults" datasource="#application.datasource#">
			select longitude,latitude
			from target_locations
			where id = <cfqueryparam CFSQLType="CF_SQL_INTEGER" value="#arguments.store_id#">
		</cfquery>

		<cfreturn qResults />

	    
	</cffunction>
	
	
	<cffunction name="getProductv3Details" access="remote" output="false" returnType="any">
		<cfargument name="dpci" type="string" required="true">

		
	
	    <cfhttp url="http://#variables.prodstatus#/products/v3/#arguments.dpci#?id_type=DPCI&fields=pricing,images,descriptions&key=#variables.targetKey#" result="searchResults" method="GET" timeout="20000">
			<cfhttpparam type="header" name="Accept" value="application/json">
		</cfhttp>
	
		<cfset result = deserializeJSON(searchResults.filecontent) >
		
		
			<cfset var results = queryNew("title,price,image,url,description")>
			
			<cftry>
			<cfif structKeyExists(result.product_composite_response.items[1], "errors")>
				<cfset results = results>
				<cfreturn results />
			</cfif>
			<cfcatch type="any">
			<cfset results = results>
			<cfreturn results />
			</cfcatch>
			</cftry>

			
			<cfif structKeyExists(result.product_composite_response.items[1], "general_description")>
				<cfset product_name = "#result.product_composite_response.items[1].general_description#">
			<cfelse>
				<cfset product_name = "N/A">			
			</cfif>
			
			<cfif structKeyExists(result.product_composite_response.items[1], "online_price")>
				<cftry>
				<cfset product_price = "#result.product_composite_response.items[1].online_price.current_price#">
				<cfcatch type="any"><cfset product_price = "N/A"></cfcatch>
				</cftry>
			<cfelse>
				<cfset product_price = "N/A">			
			</cfif>
			
			<cfif structKeyExists(result.product_composite_response.items[1], "data_page_link")>
				<cfset product_url = "#result.product_composite_response.items[1].data_page_link#">
			<cfelse>
				<cfset product_url = "http://www.target.com/s?searchTerm=#arguments.dpci#">			
			</cfif>
			
			<cftry>
			<cfif structKeyExists(result.product_composite_response.items[1], "image")>
				<cfset product_image = "#result.product_composite_response.items[1].image.external_primary_image_url[1]#">
			<cfelse>
				<cfset product_image = "">			
			</cfif>
			<cfcatch type="any"><cfset product_image = ""></cfcatch>
			</cftry>
			
			<cfif structKeyExists(result.product_composite_response.items[1], "alternate_description")>
				<cfloop index="x" from="1" to="#arrayLen(result.product_composite_response.items[1].alternate_description)#">
					<cfif result.product_composite_response.items[1].alternate_description[x].type eq "DETL">
						<cfset product_desc = "#result.product_composite_response.items[1].alternate_description[x].value#">
					<cfelse>
						<cfset product_desc = "No Description Available">
					</cfif>
				</cfloop>				
			</cfif>
			
			
			
			
			<cfset queryAddRow(results)>
			<cfset querySetCell(results, "title", product_name)>
			<cfset querySetCell(results, "price", product_price)>
			<cfset querySetCell(results, "image", product_image)>
			<cfset querySetCell(results, "url", product_url)>
			<cftry>
			<cfset querySetCell(results, "description", product_desc)>
			<cfcatch type="any"><cfset querySetCell(results, "description", "No Description Available")></cfcatch>
			</cftry>
	
	    <cfif results.recordcount gt 0>
	    	<cfreturn results />
		<cfelse>
			<cfset results = results>
			<cfreturn results />
	    </cfif>
   
	</cffunction>
	
	<cffunction name="getProductv3StorePrice" access="remote" output="false" returnType="any">
		<cfargument name="dpci" type="string" required="true">
		<cfargument name="store_id" type="string" required="true">
		
	
	    <cfhttp url="http://#variables.prodstatus#/products/v3/#arguments.dpci#?id_type=DPCI&store_id=#arguments.store_id#&fields=pricing,images,locations&key=#variables.targetKey#" result="searchResults" method="GET" timeout="200">
			<cfhttpparam type="header" name="Accept" value="application/xml">
		</cfhttp>
	
		<cfset result = xmlParse(searchResults.filecontent)>

		<cftry>
		<cfif structKeyExists(result.product_composite_response.items.product_composite, "store_price")>
			<cfset theCurrentPrice = result.product_composite_response.items.product_composite.store_price.XmlText>
			
			<cfif structKeyExists(result.product_composite_response.items.product_composite.store_price, "original_price")>
				<cfset theOriginalPrice = result.product_composite_response.items.product_composite.store_price.original_price.XmlText>
			<cfelse>
				<cfset theOriginalPrice = "">
			</cfif>
			
			<cfif len(theCurrentPrice) gt 0>
				<cfset results = theCurrentPrice>
			<cfelseif len(theCurrentPrice) eq 0 and len(theOriginalPrice) gt 0>
				<cfset results = theOriginalPrice>
			<cfelse>
				<cfset results = "N/A">
			</cfif>
		<cfelse>
			<cfset results = "N/A">
		</cfif>
		<cfcatch type="any"><cfset results = "N/A"></cfcatch>
		</cftry>

		<cfreturn results />

	    
	</cffunction>
	
	

	
	<cffunction name="checkProduct" access="remote" output="false" returnType="any" secureJSON="false">
	<cfargument name="dpci" type="string" required="true">
		
	
			<cfhttp url="http://api.target.com/products/v3/#arguments.dpci#?id_type=dpci&fields=images,descriptions,pricing&key=#variables.targetKey#" result="searchResults" method="GET" timeout="90">
				<cfhttpparam type="header" name="Accept" value="application/json">
			</cfhttp>
			
			<cfset var results = queryNew("title,price,upc,image,url,description")>
			
			<cfset result = deserializeJSON(searchResults.filecontent) >
			
			<cfif structKeyExists(result.product_composite_response.items[1], "errors")>
				<cfset results = results>
				<cfreturn results />
			<cfelse>

			
			<cfif structKeyExists(result.product_composite_response.items[1], "alternate_description")>
				<cfloop index="x" from="1" to="#arrayLen(result.product_composite_response.items[1].alternate_description)#">
					<cfif result.product_composite_response.items[1].alternate_description[x].type_description eq "Vendor Description">
						<cfset product_name = "#result.product_composite_response.items[1].alternate_description[x].value#">
					<cfelse>
						<cfset product_name = "#result.product_composite_response.items[1].general_description#">
					</cfif>
				</cfloop>				
			</cfif>
			
			<cfset queryAddRow(results)>
			<cfset querySetCell(results, "title", product_name)>
			<cfset querySetCell(results, "price", "")>
			<cfset querySetCell(results, "upc", "")>
			<cfset querySetCell(results, "image", "http://www.brickpicker.com/images/no_image_thumb.png")>
			<cfset querySetCell(results, "url", "http://www.target.com/s?searchTerm=#arguments.dpci#")>
			<cfset querySetCell(results, "description", "No Description Available")>
			</cfif>
		    <cfif results.recordcount gt 0>
		    	<cfreturn results />
			<cfelse>
				<cfset results = results>
				<cfreturn results />
		    </cfif>
		
	</cffunction>
	
	
	<cffunction name="checkProductTest" access="remote" output="false" returnType="any" secureJSON="false">
	<cfargument name="dpci" type="string" required="true">
		
			<cfhttp url="http://api.target.com/products/v3/#arguments.dpci#?id_type=dpci&fields=images,descriptions,pricing&key=#variables.targetKey#" result="searchResults" method="GET" timeout="90">
				<cfhttpparam type="header" name="Accept" value="application/json">
			</cfhttp>
			
			<cfset var results = queryNew("title,price,upc,image,url,description")>
			
			<cfset result = deserializeJSON(searchResults.filecontent) >
			
			<cfif structKeyExists(result.product_composite_response.items[1], "errors")>
				<cfset results = results>
				<cfreturn results />
			<cfelse>

			
			<cfif structKeyExists(result.product_composite_response.items[1], "alternate_description")>
				<cfloop index="x" from="1" to="#arrayLen(result.product_composite_response.items[1].alternate_description)#">
					<cfif result.product_composite_response.items[1].alternate_description[x].type_description eq "Vendor Description">
						<cfset product_name = "#result.product_composite_response.items[1].alternate_description[x].value#">
					<cfelse>
						<cfset product_name = "#result.product_composite_response.items[1].general_description#">
					</cfif>
				</cfloop>				
			</cfif>
			
			<cfset queryAddRow(results)>
			<cfset querySetCell(results, "title", product_name)>
			<cfset querySetCell(results, "price", "")>
			<cfset querySetCell(results, "upc", "")>
			<cfset querySetCell(results, "image", "http://www.brickpicker.com/images/no_image_thumb.png")>
			<cfset querySetCell(results, "url", "http://www.target.com/s?searchTerm=#arguments.dpci#")>
			<cfset querySetCell(results, "description", "No Description Available")>
			</cfif>
		    <cfif results.recordcount gt 0>
		    	<cfreturn results />
			<cfelse>
				<cfset results = results>
				<cfreturn results />
		    </cfif>
		
	</cffunction>
	
	
	<cffunction name="storeDetails" access="remote" output="false" returnType="any" secureJSON="false">
	<cfargument name="id" type="string" required="true">
		
		
	
	    <cfhttp url="http://#variables.prodstatus#/v2/store/#trim(arguments.id)#?locale=en-US&key=#variables.targetKey#" result="searchResults" method="GET" charset="utf-8">
			<cfhttpparam type="header" name="Accept" value="application/json">
		</cfhttp>
		
		<cfset var results = queryNew("store_id,store_name,addressline1,city,countryname,county,formattedaddress,latitude,longitude,postalcode,subdivision,telephone")>
	
		<cfset result = deserializeJSON(searchResults.filecontent) >
		<cfset entry = result.Location>
		<cfset queryAddRow(results)>
		<cfset querySetCell(results, "store_id", entry.id)>
		<cfset querySetCell(results, "store_name", entry.name)>
		<cfset querySetCell(results, "addressline1", entry.Address.AddressLine1)>
		<cfset querySetCell(results, "city", entry.Address.City)>
		<cfset querySetCell(results, "countryname", entry.Address.CountryName)>
		<cfset querySetCell(results, "county", '')>
		<cfset querySetCell(results, "formattedaddress", entry.Address.FormattedAddress)>
		<cfset querySetCell(results, "latitude", entry.Address.Latitude)>
		<cfset querySetCell(results, "longitude", entry.Address.Longitude)>
		<cfset querySetCell(results, "postalcode", entry.Address.PostalCode)>
		<cfset querySetCell(results, "subdivision", entry.Address.Subdivision)>
		<cfset querySetCell(results, "telephone", entry.TelephoneNumber[1].PhoneNumber)>	
		
	    <cfreturn entry />
	    
	</cffunction>
	
	
	<cffunction name="chkAvailNew" access="remote" output="false" returnType="any">
	<cfargument name="postcode" type="string" required="true">
	<cfargument name="dpci" type="string" required="true">	
	<cfset testxx = "">
	
	<cfsavecontent variable="strVar"><cfoutput><?xml version='1.0' encoding='UTF-8'?>
	<SaleableQuantityByLocation>
	   <Products>
	      <Product>
	         <ProductId>#arguments.dpci#</ProductId>
	         <DesiredQuantity>1</DesiredQuantity>
	      </Product>
	   </Products>
	   <Nearby>#arguments.postcode#</Nearby>
	   <Radius>75</Radius>
	   <MultiChannelOptions>
	      <MultiChannelOption>none</MultiChannelOption>
	   </MultiChannelOptions>
	</SaleableQuantityByLocation></cfoutput></cfsavecontent>  
	
	    <cfhttp url="http://api.target.com/products/v3/saleable_quantity_by_location?key=#variables.targetKey#" result="searchResults" method="POST" timeout="90">
			<cfhttpparam type="header" name="Accept" value="application/xml">
			<cfhttpparam type="body" value="#strVar#">
		</cfhttp>
		
		<cfset result = xmlParse(searchResults.filecontent) >
		
		<cfset var results = queryNew("store_id,store_name,store_address,store_phone,distance,availability_status,onhand_quantity,saleable_quantity")>
		
		<cftry>
		<cfif structKeyExists(result.SaleableQuantityByLocationResponse.Products.Product.Stores, "Store")>
			<cfset locCount = arrayLen(result.SaleableQuantityByLocationResponse.Products.Product.Stores.Store)>
			
			<cfloop index="x" from="1" to="#locCount#">
			<cfset entry = result.SaleableQuantityByLocationResponse.Products.Product.Stores.Store[x]>
			
			<cfif structKeyExists(entry, "OnhandQuantity")>
				<cfset OnhandQuantity = val(entry.OnhandQuantity.XmlText)>
			<cfelse>
				<cfset OnhandQuantity = 0>
			</cfif>
			
			<cfif structKeyExists(entry, "SaleableQuantity")>
				<cfset SaleableQuantity = val(entry.SaleableQuantity.XmlText)>
			<cfelse>
				<cfset SaleableQuantity = 0>
			</cfif>
			
			<cfset queryAddRow(results)>
			<cfset querySetCell(results, "store_id", entry.Storeid.XmlText)>
			<cfset querySetCell(results, "store_name", entry.StoreName.XmlText)>
			<cfset querySetCell(results, "store_address", entry.StoreAddress.XmlText)>
			<cfset querySetCell(results, "store_phone", entry.StoreMainPhone.XmlText)>
			<cfset querySetCell(results, "distance", entry.Distance.XmlText)>
			<cfset querySetCell(results, "availability_status", entry.AvailabilityStatus.XmlText)>
			<cfset querySetCell(results, "onhand_quantity", OnhandQuantity)>
			<cfset querySetCell(results, "saleable_quantity", SaleableQuantity)>
			</cfloop>
		    <cfreturn results />
		<cfelse>
		  <cfset results = results>
			<cfreturn results />
		</cfif>
		<cfcatch type="any"><cfset results = results><cfreturn results /></cfcatch>
		</cftry>
	    
	</cffunction>
	
	
	<cffunction name="getMapRoute" access="remote" output="false" returnType="any">
	<cfargument name="postcode" type="string" required="true">
	<cfargument name="storeAddress" type="string" required="true">	
		
	
	    <cfhttp url="http://#variables.prodstatus#/v2/location/map/road/route/image/driving?origin=#arguments.postcode#&destination=#urlencodedformat(arguments.storeAddress)#=time&key=#variables.targetKey#" result="searchResults" method="GET" getAsBinary="yes">
			<cfhttpparam type="header" name="Accept" value="Image">
		</cfhttp>
		
		<cfset result = searchResults.filecontent >


	    <cfreturn result />
	    
	</cffunction>


</cfcomponent>